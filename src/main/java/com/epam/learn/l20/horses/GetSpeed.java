package com.epam.learn.l20.horses;

@FunctionalInterface
public interface GetSpeed {
    int get(Horse horse);
}
