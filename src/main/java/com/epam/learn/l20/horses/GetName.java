package com.epam.learn.l20.horses;

@FunctionalInterface
public interface GetName {
    String get(Horse horse);
}
