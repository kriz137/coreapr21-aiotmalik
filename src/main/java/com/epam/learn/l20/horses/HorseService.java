package com.epam.learn.l20.horses;

public class HorseService {
    public static void main(String[] args) {
        Horse risak = new Horse(20, "Risak", true, true);
        Horse sizaya = new Horse(30, "Sizaya", true, false);
        printSpeed(risak, O -> O.getSpeed());
        printName(risak, O -> O.getName());

        printSpeed(sizaya, HORSE -> HORSE.getSpeed());
        printName(sizaya, U -> U.getName());
    }

    private static void printName(Horse horse, GetName getName) {
        System.out.println(getName.get(horse));
    }

    private static void printSpeed(Horse horse, GetSpeed getSpeed) {
        System.out.println(getSpeed.get(horse));
    }
}
