package com.epam.learn.l20;

public class RunnableEx implements Runnable{
    @Override
    public void run() {
        System.out.println("I am from: " + Thread.currentThread().getName());
    }
}
