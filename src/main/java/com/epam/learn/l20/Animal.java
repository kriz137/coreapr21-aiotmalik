package com.epam.learn.l20;

// if interface have only one abstract method - it's functional interface
@FunctionalInterface
public interface Animal {
    String getInfo(String name);
}

