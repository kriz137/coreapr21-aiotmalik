package com.epam.learn.l20;

import java.util.Locale;

public class Main {
    public static void main(String[] args) {
        Animal cat = new Cat();
        System.out.println(cat.getInfo("barsik"));

        Animal animal = new Animal() {
            @Override
            public String getInfo(String name) {
                return "I am animal with name " + name;
            }
        };
        // since jdk 8
        Animal animal2 = name -> "I am animal with name " + name;
        System.out.println(animal.getInfo("Bags"));
        System.out.println(animal2.getInfo("Rojet"));

        FunEx1 funEx1 = new RealFunc();
        funEx1.getInfo("barsik");

        FunEx1 funEx2 = new FunEx1() {
            @Override
            public void getInfo(String name) {
                System.out.println("Anonymous " + name);
            }};

        funEx2.getInfo("barsik");

        FunEx1 funEx3 = name -> {
            name = name.toUpperCase();
         System.out.println("I am lyambda realization " + name);};
        funEx3.getInfo("barsik");
    }
}
