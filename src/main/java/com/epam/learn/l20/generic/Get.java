package com.epam.learn.l20.generic;

@FunctionalInterface
public interface Get<T> {
    T get(Horse horse);
}
