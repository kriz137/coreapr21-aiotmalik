package com.epam.learn.l20.generic;

public class Horse {
    private int speed;
    private String name;
    private boolean isHungry;
    private boolean isHerbivore;

    @Override
    public String toString() {
        return "Horse{" +
                "name='" + name + '\'' +
                '}';
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHungry(boolean hungry) {
        isHungry = hungry;
    }

    public void setHerbivore(boolean herbivore) {
        isHerbivore = herbivore;
    }

    public int getSpeed() {
        return speed;
    }

    public String getName() {
        return name;
    }

    public boolean isHungry() {
        return isHungry;
    }

    public boolean isHerbivore() {
        return isHerbivore;
    }

    public Horse(int speed, String name, boolean isHungry, boolean isHerbivore) {
        this.speed = speed;
        this.name = name;
        this.isHungry = isHungry;
        this.isHerbivore = isHerbivore;
    }
}
