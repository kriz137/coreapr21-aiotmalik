package com.epam.learn.l20.generic;

public class HorseService {
    public static void main(String[] args) {
       Horse byraya = new Horse(20, "Byraya", true, true);
       Horse syzaya = new Horse(30,"Syzaya", true, false);

       print(byraya, O -> O.getSpeed());
       print(byraya, O -> O.getName());
       print(syzaya, O -> O.getSpeed());
       print(syzaya, O -> O.getName());
    }

    private static void print(Horse horse, Get get){
        System.out.println(get.get(horse));
    }
}
