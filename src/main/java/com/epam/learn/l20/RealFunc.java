package com.epam.learn.l20;

public class RealFunc implements FunEx1{
    @Override
    public void getInfo(String name) {
        name = name.toUpperCase();
        System.out.println("I am real class " + name);
    }
}
