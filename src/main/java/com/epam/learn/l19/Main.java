package com.epam.learn.l19;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
//    Executor

    public static void main(String[] args) {
//        ExecutorService executor = Executors.newFixedThreadPool(5);
//        for (int i = 0; i < 20; i++) {
//            executor.execute(() -> {
//                System.out.println(Thread.currentThread().getName());
//                Random random = new Random();
//                int count = random.nextInt(5);
//                try {
//                    TimeUnit.SECONDS.sleep(count);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            });
//        }
//        executor.shutdown();
//
        ExecutorService executor = Executors.newCachedThreadPool();
        for (int i = 0; i < 10; i++) {
            executor.execute(() -> {
                System.out.println("Hello" + Thread.currentThread().getName());
            });
        }
        executor.shutdown();
    }
}