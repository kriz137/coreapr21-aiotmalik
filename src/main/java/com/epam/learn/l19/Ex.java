package com.epam.learn.l19;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Ex implements Runnable {
    private int i = 0;

    public static void main(String[] args) {
        ExecutorService executor = Executors.newCachedThreadPool();
        Ex ex = new Ex();
        executor.execute(ex);

        while (true) {
            int val = ex.getValue();
            if (val % 2 != 0) {
                System.out.println(val);
                System.exit(0);
            }
        }
    }

    public synchronized void evenIncrement() {
        // get; add +1

        i++;
        i++;
    }

    @Override
    public void run() {
        evenIncrement();
    }

    public int getValue() {
        return i;
    }
}
