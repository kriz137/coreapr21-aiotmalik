package com.epam.learn.l19;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicEx implements Runnable {
    AtomicInteger i = new AtomicInteger();

    @Override
    public void run() {

    }

    public int getValue() {
        return i.get();
    }

    private void evenIncrement() {
        i.addAndGet(2);
    }

    public static void main(String[] args) {
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                System.err.println("Aborting...");
                System.exit(0);
            }
        }, 5000);

        ExecutorService executor = Executors.newCachedThreadPool();
        com.epam.learn.l18.AtomicEx ex = new com.epam.learn.l18.AtomicEx();
        executor.execute(ex);

        while (true) {
            int val = ex.getValue();
            System.out.println(val);
            if (val % 2 != 0) {
                System.out.println(val);
                System.exit(0);
            }
        }
    }
}
