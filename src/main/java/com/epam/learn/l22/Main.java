package com.epam.learn.l22;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Stream.empty(); // пустой стрим строк
        List<String> list = new ArrayList<>();
        list.stream();
        Map<String,String> map = new HashMap<>();
        map.entrySet().stream(); // Stream<Map.Entry<String,String>>
    }
}
