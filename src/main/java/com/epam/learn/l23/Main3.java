package com.epam.learn.l23;

import java.time.Duration;
import java.time.Instant;

public class Main3 {
    public static void main(String[] args) {
        Instant start = Instant.now();
        System.out.println("some logic");
        for (int i = 0; i < 1000000000; i++) {
            int j = (i - 10) / (i + 1);
        }
        Instant end = Instant.now();
        Duration timeElapsed = Duration.between(start, end);
        System.out.println(timeElapsed.toMillis());
    }
}
