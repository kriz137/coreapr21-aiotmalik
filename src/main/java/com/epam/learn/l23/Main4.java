package com.epam.learn.l23;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.TemporalAmount;

public class Main4 {
    public static void main(String[] args) {
        LocalDate today = LocalDate.now();
        System.out.println(today);

        LocalDate barsikBirthday = LocalDate.of(1999, 3, 15);
        barsikBirthday = barsikBirthday.plusYears(2);
        barsikBirthday = barsikBirthday.minusWeeks(2);
        TemporalAmount amount = Period.ofDays(2);
        barsikBirthday = barsikBirthday.plus(amount);
        System.out.println(barsikBirthday);

        barsikBirthday = barsikBirthday.withDayOfMonth(2);
        System.out.println(barsikBirthday);

        barsikBirthday = barsikBirthday.withDayOfYear(2);
        System.out.println(barsikBirthday);

        barsikBirthday = barsikBirthday.withMonth(2);
        System.out.println(barsikBirthday);

        barsikBirthday = barsikBirthday.withYear(2);
        System.out.println(barsikBirthday);
        System.out.println(barsikBirthday.getDayOfYear());

        barsikBirthday.getMonth();
        barsikBirthday.getMonthValue();
        barsikBirthday.getYear();
//        Period period = barsikBirthday.until()
        barsikBirthday.isLeapYear();

    }
}
