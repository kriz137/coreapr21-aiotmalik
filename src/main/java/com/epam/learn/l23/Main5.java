package com.epam.learn.l23;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.GregorianCalendar;

public class Main5 {
    public static void main(String[] args) {
        LocalTime localTime = LocalTime.of(10, 2);
        localTime = localTime.plusSeconds(55555);
        System.out.println(localTime);

        long value = localTime.toEpochSecond(LocalDate.now(), ZoneOffset.MIN);
        System.out.println(value);
        LocalDateTime localDateTime;

        ZonedDateTime zonedDateTime = ZonedDateTime.of(2020, 2, 2, 2,
                2, 2, 2, ZoneId.of("UTC-2"));
        System.out.println(zonedDateTime);

        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG);
        String format = formatter.format(ZonedDateTime.now());
        System.out.println(format);

        final Instant now1 = Instant.now();
        final Date date = Date.from(now1);
        System.out.println(date.toInstant());
        System.out.println(now1);

        ZonedDateTime now2 = ZonedDateTime.now();
        GregorianCalendar calendar = GregorianCalendar.from(now2);
        System.out.println(calendar.toZonedDateTime());
        System.out.println(now2);

        LocalDateTime now = LocalDateTime.now();
        Timestamp timestamp = Timestamp.valueOf(now);
        System.out.println(timestamp.toLocalDateTime());
        System.out.println(now);

        LocalDate now3 = LocalDate.now();
        java.sql.Date date1 = java.sql.Date.valueOf(now3);
        System.out.println(now3);
        System.out.println(date1.toLocalDate());

        LocalTime now4 = LocalTime.now();
        Time time = Time.valueOf(now4);
        System.out.println(now4);
        System.out.println(time.toLocalTime());
    }
}
