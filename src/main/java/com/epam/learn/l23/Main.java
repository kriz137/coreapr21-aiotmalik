package com.epam.learn.l23;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        // legacy
        Date date = new Date();// текущее время
        System.out.println(date.getTime());

        Date date2 = new Date(0); // 1 Января 1970 Jan 01 1970
        System.out.println(date2);

        // dd-M-yyyy hh:mm:ss
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String dateString = "31-05-1985 10:20:56";
        Date date3 = null;
        try {
            date3 = sdf.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(date3);
    }
}
