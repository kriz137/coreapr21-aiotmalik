package com.epam.learn.l18;

import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Ex implements Runnable{
    private int i = 0;

    public synchronized int getValue() {
        return i;
    }

    private synchronized void evenIncrement(){
        i++;
        i++;
    }

    @Override
    public void run() {
      while (true) {
          evenIncrement();
      }
    }

    public static void main(String[] args) {
        ExecutorService executor = Executors.newCachedThreadPool();
        Ex ex = new Ex();
        executor.execute(ex);

        while (true){
            int value = ex.getValue();

            if (value %2 != 0){
                System.out.println(value);
                System.exit(0);

            }
        }
    }
}
