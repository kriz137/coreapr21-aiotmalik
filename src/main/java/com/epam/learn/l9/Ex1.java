package com.epam.learn.l9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ex1 {
    public static void main(String[] args) {
        System.out.println("Hello!");
//        throw new RuntimeException("I am exception");
//        System.out.println("By!");
//        throw -выброс исключения
//        trows - перекладываем ответственность обработки ислючения на кого-то ещё
//        try - попытка
//        catch - отлавливаем исключительное событие
//        finally - блок кода который будет выполняться всегда

        try {
            throw new RuntimeException("I am exception");
        }catch (RuntimeException e){
//        }catch (Error e) - скорее всего программа не дойде до следующего шага
//            System.err.println(e.getMessage());
            e.printStackTrace();
        }finally {
            System.out.println("Finally");
        }
        System.out.println("By");

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        }
    void getMoney() throws IOException {
        throw new IOException();
    }

    void getMoney2() throws RuntimeException {
        throw new RuntimeException();
    }
    void getInfo(){
        try {
            getMoney();
        } catch (IOException e) {
            e.printStackTrace();
        }
        getMoney2();
    }
}
