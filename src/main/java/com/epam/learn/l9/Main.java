package com.epam.learn.l9;

public class Main {
    public static void main(String[] args) {
        Optional<Integer> ob1 = new Optional<>();
        ob1.setValue(1);
//        ob1.setValue("1");
        int v1 = ob1.getValue();

        Optional<String> ob2 = new Optional<>("Java");
        String v2 = ob2.getValue();

//        ob1 = ob2;
        Optional ob3 = new Optional();
        ob3.setValue("Java SE");
        ob3.setValue(71);
        ob3.setValue(null);
    }
}
