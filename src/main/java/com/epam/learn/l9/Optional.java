package com.epam.learn.l9;

public class Optional<T> {
    private T value;

    public Optional(T value) {
        this.value = value;
    }

    public Optional() {
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public String toString() {
        if (value == null) {
            return null;
        }
        return "Optional{" +
                "value=" + value +
                '}';
    }


}

