package com.epam.learn.l9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ex2 {

    public static void main(String[] args) {
        try {
            // open connection
            throw new RuntimeException();

        } catch (RuntimeException e) {
            //
        } finally {
//            System.out.println("close connection");
            //close connection
//            System.err.println("closing resources");
        }

//        try {
//            System.out.println("open connection");
//            throw new RuntimeException();
//        } finally {
//            System.out.println("close connection");
//        }

//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//        try {
//            System.out.println(reader.readLine());
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                reader.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
