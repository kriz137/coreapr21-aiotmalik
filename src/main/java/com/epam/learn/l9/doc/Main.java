package com.epam.learn.l9.doc;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        List<? extends Doctor> list = new ArrayList<MedicalStaff>();
        List<? extends Doctor> list = new ArrayList<>();
//        list.add(new Doctor()); нельзя
        List<? extends Doctor> list2 = new ArrayList<HeadDoctor>();
//        list2.add(new Doctor()); нельзя
        List<? super Doctor> list3 = new ArrayList<Doctor>();
        List<? super Doctor> list4 = new ArrayList<MedicalStaff>();
        List<? super Doctor> list5 = new ArrayList<Object>();

//        list4.add(new Object());
//        list4.add(new MedicalStaff());
        list4.add(new Doctor());
        list4.add(new HeadDoctor());

        Object object = list4.get(0);
//        MedicalStaff medicalStaff = (MedicalStaff) list4.get(0);
//        Doctor doctor = list4.get(0);
//        HeadDoctor headDoctor = list4.get(0);

    }
}
