package com.epam.learn.l9.doc;

public class Ex {
    //    < T extends Тип> Тип method (T arg) {}
//    <T> Тип method (T arg) {}
    public static void main(String[] args) {
        System.out.println(asByte(7));
        System.out.println(asByte(7.f));
//        System.out.println(asByte(new Character('7')));
    }

    public static <T extends Number> byte asByte(T num) {
        long n = num.longValue();
        if (n >= -128 && n <= 127) {
            return (byte) n;
        } else {
            return 0;
        }
    }

    public static <Type> void method(Type obj) {
        System.out.println(1);
    }

    public static void method(Number obj) {
        System.out.println(1);
    }

    public static void method(Integer obj) {
        System.out.println(1);
    }
}
