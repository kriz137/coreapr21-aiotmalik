package com.epam.learn.l9;

public class OptionalQ<T extends Number> {
    public T mark;

    public OptionalQ(T mark) {
        this.mark = mark;
    }

    public int roundMark() {
        return Math.round(mark.floatValue());
    }

    public T getMark() {
        return mark;
    }

    public void setMark(T mark) {
        this.mark = mark;
    }

    public boolean someAny(OptionalQ<?> ob) {
        return roundMark() == ob.roundMark();
    }
    public boolean same (OptionalQ<T> ob){
        return getMark() == ob.getMark();
    }
}
