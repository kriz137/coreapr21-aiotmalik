package com.epam.learn.l12;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Ex1 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Barsik");
        list.add("Murzik");
        list.add("Barsik");
        list.add("Barsik");
        list.add("Murzik");
        list.add("Barsik");

//        Never DELETED ELEMENTS FROM LIST WITH FOREACH, FORI
//        for (String name : list) {
//            if (name.equalsIgnoreCase("Barsik")) {
//                list.remove("Barsik");
//            }
//        }
//        USE ITERATOR for removing element!
        // before Java 8
//        Iterator<String> iterator = list.listIterator();
//        while (iterator.hasNext()){
//            if (iterator.next().equalsIgnoreCase("Barsik")){
//                iterator.remove();
//            }
//        }
        // after Java 8
        list.removeIf(O -> O.equals("Barsik"));
        System.out.println(list);
    }
}
