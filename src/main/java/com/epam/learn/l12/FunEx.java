package com.epam.learn.l12;

@FunctionalInterface
public interface FunEx {
    public abstract String getInfo();
}
