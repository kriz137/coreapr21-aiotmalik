package com.epam.learn.l12;

@FunctionalInterface
public interface CatInterface {
    static void getName(){
        System.out.println("I am Barsik");
    }

    default void getName2(){
        System.out.println("I am from Interface");
    }
    public abstract String getInfo(String name);
}
