package com.epam.learn.l12;

public interface AnimalInterface {
    default void getName2(){
        System.out.println("I am from Interface2");
    }

    default void getEat(){
        System.out.println("I am eating");
    }

}
