package com.epam.learn.l12;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("Barsik");
        list.add("Murzik");

        System.out.println(list.get(1));
        list.set(1, "Snegok");
        list.add(1, "Snegok2");
        System.out.println(list);
        System.out.println(list.size());

//        List<Integer>list2 = new ArrayList<>();
//        list2.add(1);
//        list2.add(2);
//
//        System.out.println(list2.get(1));
////        Vector vector;
////        Stack stack;
////        ArrayList arrayList;
////        Object[] arrayList = new Object[];
//        LinkedList linkedList;
    }
}
