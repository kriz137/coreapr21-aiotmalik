package com.epam.learn.l12;

import java.util.LinkedList;

public class Ex3 {
    public static void main(String[] args) {
        LinkedList<String> linkedList = new LinkedList<>();
        linkedList.add("barsik");
        linkedList.add("mursik");
        linkedList.add(1, "Yki");

        linkedList.addFirst("snegok");
        linkedList.addLast("snegok");
        linkedList.removeFirst();
        linkedList.removeLast();
        linkedList.getFirst();
        linkedList.getLast();
    }
}
