package com.epam.learn.l16;

public class Main {
    public static void main(String[] args) {
        Duck mallardDuck = new MallardDuck();

//        mallardDuck.performFly(new FlyWithWings());
        mallardDuck.performFly();

        Duck toy = new ToyDuck();
//        toy.performFly(new FlyNoWay());
        toy.performFly();

    }
}
