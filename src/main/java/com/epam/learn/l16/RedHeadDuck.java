package com.epam.learn.l16;

public class RedHeadDuck extends Duck{
    @Override
    void display() {
        System.out.println("I am read head duck");
    }
}
