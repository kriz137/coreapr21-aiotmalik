package com.epam.learn.l16;

public class ToyDuck extends Duck{

    public ToyDuck() {
        flyable = new FlyNoWay();
    }

    @Override
    void display() {
        System.out.println("toy");
    }
}
