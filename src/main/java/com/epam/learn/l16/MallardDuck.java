package com.epam.learn.l16;

public class MallardDuck extends Duck{
//    public MallardDuck() {
//        flyable = new FlyWithWings();
//    }

    public MallardDuck() {
        flyable = new FlyWithWings();
    }

    @Override
    void display() {
        System.out.println("I am mallard duck");
    }
}

