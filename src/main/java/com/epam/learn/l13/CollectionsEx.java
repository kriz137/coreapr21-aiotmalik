package com.epam.learn.l13;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CollectionsEx {
    public static <CuncurrentSkipListSet> void main(String[] args) {
//        Arrays
//        Collections.sort(); сортировка
//        Collections.binarySearch() поиск делением
//        Collections.reverse(); развернуть коллекцию
//        Collections.shuffle(); перемешать элементы
//        Collections.fill(); заменить каждый(все) элемент заданным обьектом
        List<String> list = new ArrayList<>();
        list.add("Barsik");
        list.add("Bars");
        list.add("Murzik");
        list.add("Murzik1");
        list.add("Murzik2");
        list.add("Murzik3");
        list.add("Murzik4");
//        Collections.fill(list,"Snegok");
        System.out.println(list);
        System.out.println(Collections.max(list)); // максимальный элемент коллекции
        System.out.println(Collections.min(list)); // минимальный элемент коллекции
//        Collections.rotate(); //повернуть список на указаное число элементов
        Collections.replaceAll(list, "Barsik", "Bars");
        System.out.println(list);
        Collections.swap(list, 1, 4);// поменять местами
        System.out.println(list);
//        Collections.unmodifiableCollection() трансформация коллекции в не изменяемую копию
        Collection listSet = Collections.synchronizedCollection(list);
        System.out.println(listSet);
        System.out.println(Collections.frequency(list, "Bars"));//показывает колличество элементов с таким именим
//        Collections.disjoint() проверяет ли есть в 2х коллекциях одинаковые элементы
//        Collections.addAll() может добавить все элементы из массива в колекци
//        Collections.newSetFromMap() сделать Set из Map


    }
}
