package com.epam.learn.l13;

import java.util.TreeMap;

public class TreeMapEx {
    public static void main(String[] args) {


        TreeMap<String, String> map = new TreeMap<>();
        map.put("Barsik", "Barsik");
        map.put("Murzik", "Murzik");
        System.out.println(map.firstKey());
        System.out.println(map.lastKey());
    }
}
