package com.epam.learn.l13;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Main {
    public static void main(String[] args) {
        Map<String,String> map = new HashMap<>();
//        Hashtable hashtable; не используется. потокобезопасный  ConcurrentHashMap вместо
//        HashMap hashMap;
//        LinkedHashMap linkedHashMap;
//        SortedMap sortedMap;
//        NavigableMap navigableMap;
//        TreeMap treeMap;

        map.put("Barsik", "Barsik the cat");
        map.put(null, "Barsik the cat");
        map.get("Barsik");
        System.out.println(map.containsKey("Barsik"));
        System.out.println(map.containsValue("Barsik the cat"));
        System.out.println(map.containsValue("Barsik the cat1"));
        System.out.println(map.size());
        map.remove("Barsik");

        System.out.println(map.containsKey(null));

        System.out.println(map.get(null));

        System.out.println(map.size());
    }
}
