package com.epam.learn.l15;

public interface CalcService {
      double sum (double val1, double val2);
}
