package com.epam.learn.l21.cats;

@FunctionalInterface
public interface CheckCat {
    boolean check(Cat cat);
}
