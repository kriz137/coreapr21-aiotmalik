package com.epam.learn.l21.exist;

import java.util.function.Supplier;

public class SupplierExample {
    public static void main(String[] args) {
        Supplier<String> supplier = () -> "Functional interface java 8";
        System.out.println(supplier.get());

        Supplier<Cat> supplier2 = () -> new Cat("Barsik");
        System.out.println(supplier2.get());

        Supplier<CatsInterface> supplier3 = () -> new CatsInterface() {
            @Override
            public String toString() {
                return "Barsilio";
            }
        };
        System.out.println(supplier3.get());
    }
}
