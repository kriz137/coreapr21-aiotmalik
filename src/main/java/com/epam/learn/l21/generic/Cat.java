package com.epam.learn.l21.generic;

public class Cat {
    private String name;
    private boolean isHungry;
    private boolean isHerbivore;

    public int getSpeed() {
        return speed;
    }

    private int speed;

    public Cat(int speed, String name, boolean isHungry, boolean isHerbivore) {
        this.speed = speed;
        this.name = name;
        this.isHungry = isHungry;
        this.isHerbivore = isHerbivore;
    }

    public String getName() {
        return name;
    }

    public boolean isHungry() {
        return isHungry;
    }

    public boolean isHerbivore() {
        return isHerbivore;
    }
}
