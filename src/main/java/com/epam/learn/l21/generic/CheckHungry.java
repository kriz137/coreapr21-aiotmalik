package com.epam.learn.l21.generic;

@FunctionalInterface
public interface CheckHungry {
    boolean get(Cat cat);
}
