package com.epam.learn.l21.generic;

@FunctionalInterface
public interface CheckSpeed {
    int get(Cat cat);
}
