package com.epam.learn.l21.generic;

@FunctionalInterface
public interface Check<T> {
    T get(Cat cat);
}
