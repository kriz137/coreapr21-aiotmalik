package com.epam.learn.l8;

public class PizzaRunner {
    private PizzaInterface pizzaMsk;
    private PizzaInterface pizza;
    private PizzaInterface pizzaSPb;

    public static void main(String[] args) {
        PizzaRunner runner = new PizzaRunner();
        runner.getPizzaInfo();
    }

    private void getPizzaInfo() {
        if (true) {
            pizza = new PizzaMSK();
        }else {
            pizza = new PizzaSPb();
        }

        pizzaMsk = new PizzaMSK();
        pizzaSPb = new PizzaMSK();
//        pizzaSPb.sayHello();
        pizzaMsk.wash();
        pizzaSPb.wash();
        pizzaMsk.cook();
        pizzaSPb.cook();
        pizzaMsk.delivery();
        pizzaSPb.delivery();
    }
}
