package com.epam.learn.l8;

public class Person implements Comparable<Person> {
    private String firstName;
    private String lastName;
    private int age;

    public static void main(String[] args) {
        Person person1 = new Person();
        person1.setAge(10);
        Person person2 = new Person();
        person2.setAge(10);
        System.out.println(person2.compareTo(person1));
    }

    // person2.compareTo(person1) ==
    @Override
    public int compareTo(Person anotherPerson) {
        int anotherPersonAge = anotherPerson.age;
        return this.age - anotherPersonAge;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
