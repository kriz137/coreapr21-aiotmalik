package com.epam.learn.l8;

import java.util.Date;

public class Department implements Cloneable {
    private Integer codeName;
    private Date date = new Date();

    public static void main(String[] args) throws CloneNotSupportedException {
        Department department = new Department();
        department.date = new Date();

        Department department1 = (Department) department.clone();
        Department department2 = new Department();
        department2.date = department.date;

        System.out.println(department1.date);

        System.out.println(department == department1);
        System.out.println(department == department2);
        System.out.println(department1 == department2);
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Department obj;

        obj = (Department) super.clone();
        if (null != this.date) {
            obj.date = (Date) this.date.clone();
        }
        return obj;
    }
}
