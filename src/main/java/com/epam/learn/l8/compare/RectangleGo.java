package com.epam.learn.l8.compare;

public class RectangleGo extends GeometricObject {
    private double sideA;
    private double sideB;

    public RectangleGo(double sideA, double sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    @Override
    public double getArea() {
        return sideA * sideB;
    }
}
