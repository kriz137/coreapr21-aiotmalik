package com.epam.learn.l8.compare;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        Comparator<GeometricObject> comparator = new GeometricObjectComparator();
        Set<GeometricObject> set = new TreeSet<>(comparator);
        CircleGo e = new CircleGo(40);
        set.add(e);
        RectangleGo e1 = new RectangleGo(4, 5);
        set.add(e1);
        CircleGo e2 = new CircleGo(40);
        set.add(e2);
        RectangleGo e3 = new RectangleGo(4, 1);
        set.add(e3);

        System.out.println(new CircleGo(40).hashCode());
        System.out.println(new CircleGo(40).hashCode());

        System.out.println("A sorted set of geometric objects");

        for (GeometricObject s: set) {
            System.out.println("area = " + s.getArea());
        }

    }
}
