package com.epam.learn.l8.compare;

public class CircleGo extends GeometricObject{
    private double radius;

    public CircleGo(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return Math.PI * radius *radius;
    }
}
