package com.epam.learn.l8;

@FunctionalInterface
public interface FuncIn {
    void getSomething1();

    boolean equals(Object obj);
}
