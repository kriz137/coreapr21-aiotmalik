package com.epam.learn.l4;

public class Cat extends Animal {
    private String name;
    private int age;

    // ctrl + shift + alt + l
    public Cat(String name, int age) {
        super(name, age);
    }

    @Override
    public String toString() {
        return super.getName();
    }
}
