package com.epam.learn.l17;

import java.io.IOException;

public class MainVolatile {
    private static volatile boolean flag = true;

    public static void main(String[] args) {
        new TreadClassFlagReader().start();
        new TreadClassFlagSetter().start();

    }
    public static class TreadClassFlagReader extends Thread{
        @Override
        public void run() {
            System.err.println("waiting...");

            while (flag){

            }

            System.err.println("Gogogog");
        }
    }

    public static class TreadClassFlagSetter extends Thread{
        @Override
        public void run() {
            try {
                int k = System.in.read();
            } catch (IOException e) {
                e.printStackTrace();
            }

            flag = false;

            System.err.println("Flag is down");
        }
    }
}
