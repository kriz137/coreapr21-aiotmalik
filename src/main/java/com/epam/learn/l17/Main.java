package com.epam.learn.l17;

public class Main {
    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getName());

        RunnableEx ex = new RunnableEx();
        Thread thread = new Thread(ex);
        thread.start();

         ThreadEx threadEx = new ThreadEx();
        threadEx.start();

        I i = new I() {
        };

//        Thread thread2 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                System.out.println("I am anonymous class from in another thread " + Thread.currentThread().getName());
//            }
//        });
//       Thread thread2 = thread2.start();

//        тоже самое:
        Thread thread2 = new Thread(() -> System.out.println("I am anonymous class from in another thread "
                + Thread.currentThread().getName()));
        thread2.start();
//        и это такж тоже самое(упраенная запись)
        new Thread(() -> System.out.println("I am anonymous class from in another thread "
                + Thread.currentThread().getName())).start();
    }
}
