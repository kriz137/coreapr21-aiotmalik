package com.epam.learn.l7;

public class Animal {
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public void sayGav(){
        System.out.println("GAV!!!");
    }

    public String getName() {
        return name;
    }
}
