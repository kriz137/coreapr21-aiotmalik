package com.epam.learn.l7;

import java.util.Objects;

public class Snegok {
    @Override
    public String toString() {
        return "Snegok{" +
                "name='" + name + '\'' +
                '}';
    }

    public Snegok(String name) {
        this.name = name;
    }

    private String name;

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Snegok snegok = (Snegok) o;
//        return Objects.equals(name, snegok.name);
//    }

    @Override
    public int hashCode() {
        return 1;
    }
}
