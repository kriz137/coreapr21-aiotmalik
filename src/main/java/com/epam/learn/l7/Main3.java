package com.epam.learn.l7;

public class Main3 {
    public static void main(String[] args) throws CloneNotSupportedException {
//        System.out.println(Integer.MAX_VALUE);
//        System.out.println(Integer.MIN_VALUE);

        Snegok snegok1 = new Snegok("Sneg");
        Snegok sneg = new Snegok("Sneg");

        System.out.println(sneg.hashCode());
        System.out.println(snegok1.hashCode());

        System.out.println(sneg.equals(snegok1));

        System.out.println((long) Integer.MAX_VALUE - Integer.MIN_VALUE);
        System.out.println(new Main3().getClass());

        Cat cat = new Cat();

        Object barsik = cat.clone();

        System.out.println(barsik);
    }
}
