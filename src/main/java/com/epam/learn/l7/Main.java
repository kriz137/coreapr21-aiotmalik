package com.epam.learn.l7;

public class Main {
    static private int count = 20;
    static {
        count = 15;
    }
    static {
        count = 19;
    }

    public static void main(String[] args) {
        System.out.println(count);
    }
}
