package com.epam.learn.l1;

public class PrimitiveExample {
    //целые
    // -128 127
    // 8 бит
    byte someByte;
    // -2^15 2^15-1
    // 16 бит
    short someShort;
    // -2^31 2^31-1
    // 32 бит
    int number = 1;
    //-2^63 2^63-1
    // 64 бит
    long bigNumber;

    //дробные
    //IEEE 754-2008
    // 32 бит
    float nFloat;
    // 64 бит
    double nDouble;

    // символьный
    //16 бит
    char someChar;

    //логический
    // зависит от ОС
    boolean isTrue;
}
