package com.epam.learn.l14;

import java.io.*;

public class Ex {
    public static void main(String[] args) {
        Reader reader; //text
        Writer writer; //text
        InputStream inputStream; //binary
        OutputStream outputStream; //binary
//        Memory
        CharArrayReader reader1;
        CharArrayWriter writer1;
        ByteArrayInputStream arrayInputStream;
        ByteArrayOutputStream arrayOutputStream;
//        File
        FileWriter writer2;
        FileReader reader2;
        FileInputStream fileInputStream;
        FileOutputStream fileOutputStream;

//        Pipe
        PipedReader pipedReader;
        PrintWriter printWriter;
        PipedInputStream pipedInputStream;
        PipedOutputStream pipedOutputStream;
    }
}
