package com.epam.learn.l14;

import java.io.*;

public class Main {
    public static void main(String[] args) {


//    in
//    out

//    text-stream  символьные потоки 16 бит символов Юникод (I am Barsik)
//        binary-stream 8bit info [01101100][11101101]

//        open
//        working
//        close!!!

//        PrintWriter writer = null;
//        try {
//            //open stream
//            FileWriter out = new FileWriter("text.txt");
//
////            придание потоковому обьекту требуемых свойств
//            BufferedWriter bufferedWriter = new BufferedWriter(out);
////            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("text.txt"));
//
//            writer = new PrintWriter(bufferedWriter);
//
//            writer.println("I am sentence in a text file");
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (writer != null){
//                writer.close();
//            }
//        }

        try(PrintWriter writer1 = new PrintWriter(new BufferedWriter(new FileWriter("text.txt")))){
            writer1.println("I am sentence in a text file with try");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
