package com.epam.learn.l14;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Ex3 {
    public static void main(String[] args) throws IOException {
        byte[] bytesToWrite = {1, 2, 3};
        byte[] bytesToRead = new byte[10];
        String fileName = "c:\\test.txt";
        FileOutputStream outputStream = null;
        FileInputStream inputStream = null;

        outputStream = new FileOutputStream(fileName);
        System.out.println("file is opened for writing...");
        outputStream.write(bytesToWrite);
        System.out.println("wrote: " + bytesToWrite.length +" byte");
        outputStream.close();
        System.out.println("Finished");

        inputStream = new FileInputStream(fileName);
        System.out.println("File opened for reading...");
        int byteAvaibale = inputStream.available();
        System.out.println("Ready for reading: " + byteAvaibale + "byte");
        int count = inputStream.read(bytesToRead,0,byteAvaibale);
        System.out.println("Read: " + count + " byte");
        inputStream.close();
        System.out.println("Finished in");
    }
}
