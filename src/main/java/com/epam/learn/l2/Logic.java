package com.epam.learn.l2;

public class Logic {
    // default = false
    private boolean paramA = true;
    private boolean paramB = false;
    private boolean paramC = true;

    public static void main(String[] args) {
        Logic logic = new Logic();
        System.out.println(logic.checkResultFirst(logic.paramA, logic.paramB, logic.paramC));

    }

    // &&, ||, >, <, ==, !=, <=, >=

    // A && B || C
    private boolean checkResultFirst(boolean a, boolean b, boolean c){
        return a && b || c;
    }

    // 1) A && B
    // 2) C || B
    // 3) A || B && C

}
