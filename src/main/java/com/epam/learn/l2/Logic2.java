package com.epam.learn.l2;

public class Logic2 {

    private boolean paramA = true;
    private boolean paramB = false;
    private boolean paramC = true;

    public static void main(String[] args) {
        Logic2 logic2 = new Logic2();
        System.out.println(logic2.checkResultFirst(logic2.paramA,logic2.paramB));
        System.out.println(logic2.checkResultSecond(logic2.paramA,logic2.paramB));
        System.out.println(logic2.checkResultThird(logic2.paramA, logic2.paramB, logic2.paramC));

    }

    private boolean checkResultFirst(boolean a, boolean b){
        return a && b;
    }

    private boolean checkResultSecond(boolean a, boolean b){
        return a || b;
    }

    private boolean checkResultThird(boolean a, boolean b, boolean c){
        return a || b && c;
    }

}
