package com.epam.learn.l2;

public class StaticInitial {
    private int count;
    private Cat cat;

    public StaticInitial(int count, Cat cat) {
        this.count = count;
        this.cat = cat;
    }

    public StaticInitial(){
    }

    public String getName(){
        return cat.getName();
    }
}
