package com.epam.learn.l15;

import org.junit.*;

public class CalcTest {
    private Calc calc;

//
//    @BeforeClass
//    public static void globalInt() {
//
//    }
//
//    @AfterClass
//    public static void globalCleanUp() {
//
//    }
//
//    @Before
//    public void init() {
//        calc = new Calc();
//    }
//
//    @After
//    public void cleanUp() {
//
//    }

    @Test(expected = RuntimeException.class)
    public void testSum() {
//        try {
//            TimeUnit.MILLISECONDS.sleep(1);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        Assert.assertEquals(15.0, calc.sum(10, 5), 0);
        throw new RuntimeException();
    }
}
